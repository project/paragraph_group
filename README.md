[[_TOC_]]


#### Introduction
The Paragraph Group module integrates the functionality of the Paragraphs and
Field Group modules, and makes nested groups of Paragraphs easier to navigate,
edit and manage. It does this by offering 5 key features:

1. An improved editing widget for Paragraph fields (called 'Paragraph
Details'), based on the HTML Details element. This widget makes nested
paragraphs easier to edit.
2. An Administrative Title Field for Paragraphs which integrates with the above
Paragraph Details widget, making it easy to add a summary field to Paragraphs.
3. A template for applying a set of Field Groups to all Content Types meeting
the minimum requirements. In particular, the Paragraphs field group created by
this functionality also integrates with the Paragraph Details widget through
an 'Open All Paragraphs' button, which appears in the Paragraphs group tab
when applicable. This makes all Paragraph fields easier to manage.
4. The Paragraph Group module settings page makes it easy apply these features
to all the relevant site components they can be applied to. Through the
settings page, site administrators can apply these features by ticking
checkboxes on one page, instead of navigating through every entity type they
want to apply these features to.
5. The settings page also enables you to setup bespoke styling modifications
in order to maximise the compatibility of your Administration Theme with both
the Paragraphs and Paragraph Group modules. 

Through these 5 features, the Paragraph Group module can significantly improve
the content authoring experience of many Drupal websites, particularly if large
sets of Paragraphs, Content Types and Fields are becoming difficult to manage.

- For a full description of the module, visit the project page:
https://www.drupal.org/project/paragraph_group
- To submit bug reports and feature suggestions, or track changes:
https://www.drupal.org/project/issues/paragraph_group


#### Requirements
This module requires the following contributed modules:
- [Paragraphs](https://www.drupal.org/project/paragraphs)
- [Field Group](https://www.drupal.org/project/field_group)

The module is now compatible with 3 Administration Themes:
[Gin Theme](https://www.drupal.org/project/gin)
[Claro Theme](https://www.drupal.org/project/claro)
[Seven Theme](https://www.drupal.org/project/seven-theme)


#### Installation
Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


#### Configuration
The module has one settings / configuration page at Administration »
Configuration » Content authoring » Paragraph Group module settings, or at the
path /admin/config/content/paragroup.

This project is tested with BrowserStack.
