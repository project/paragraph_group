function paragroup($, context, settings) {

  let functions = {

    /**
     * Scrolls the browser window to element location.
     */
    scrollToEl: function (el) {

      // Minus 95px so element scrolls to below Admin Toolbar
      let offsetVal = el.offset().top - 95;

      let scroll = {
        scrollTop: offsetVal
      };

      // Scroll to anchor in 250ms
      $('html, body').animate(scroll, 250);

    },

    /**
     * Resets the button state to collapsed.
     */
    resetBelow: function ($button) {

      $button.hasClass('expanded') ?
        $button.click() :
        $button.click().click();

    },

    /**
     * Expands Open Below buttons in bulk.
     */
    expandBelowBulk: function () {

      let $ths = $(this);

      functions.resetBelow($ths);
      $ths.click();

    },

    /**
     * Collapses Open Below buttons in bulk.
     */
    collapseBelowBulk: function () {
      functions.resetBelow($(this));
    },

    /**
     * Sets Open Nested buttons to expanded.
     */
    setNestedExpanded: function ($nestedButtons) {

      $nestedButtons
        .removeClass('collapsed')
        .addClass('expanded')
        .val('Close Nested');

    },

    /**
     * Sets Open Nested buttons to collapsed.
     */
    setNestedCollapsed: function ($nestedButtons) {

      $nestedButtons
        .removeClass('expanded')
        .addClass('collapsed')
        .val('Open Nested');

    },

    /**
     * Resets all Open Nested buttons to collapsed.
     */
    resetAllNested: function ($button) {

      let $table = $button.closest('table');
      let $belowButtons = $table.find('.paragroup.toggle-below');
      let $nestedButtons = $table.find('.paragroup.toggle-nested');

      $belowButtons.each(function () {
        functions.resetBelow($(this));
      });

      functions.setNestedCollapsed($nestedButtons);

    },

    /**
     * Actions to carry out if button is physically clicked.
     */
    buttonClicked: function (event, $ths) {

      if(!(event.originalEvent === undefined)) {

        if($ths.hasClass('collapsed') &&
          $ths.hasClass('toggle-below')) {
          functions.resetAllNested($ths);
        }

        functions.scrollToEl($ths.siblings('a'));

      }

    },

    /**
     * Determine if a paragraph has sub-paragraphs. Used to prevent Open Nested
     * button being appended to lowest level Paragraphs.
     */
    paragraphHasSubParagraphs: function ($paragraph) {

      let draggableRows = false;

      let $details = $paragraph.closest('table')
        .find('> tbody > tr > td > div > details');

      let detailsWidgetSel =
        '.field--widget-paragraph-group-details-widget';

      let $detailsWidget = $details.find(detailsWidgetSel);

      if($detailsWidget.length) {

        let $tables = $detailsWidget.find('table');

        if($tables.length) {

          $tables.each(function () {

            let $ths = $(this);
            let $rows = $ths.find('> tbody > tr.draggable');

            if($rows.length > 1) {
              draggableRows = true;
              return false;
            }

          });

        }

      }

      return draggableRows;

    },

    /**
     * Toggles an Open Below button.
     */
    toggleBelow: function (event) {

      let $ths = $(this);
      event.preventDefault();

      let $details = $ths.closest('table')
        .find('> tbody > tr > td > div > details');

      if($ths.hasClass('collapsed')) {
        $ths.val('Close Below');
        $details.prop('open', true);
        $ths.removeClass('collapsed').addClass('expanded');
      }
      else if($ths.hasClass('expanded')) {
        $ths.val('Open Below');
        $details.prop('open', false);
        $ths.removeClass('expanded').addClass('collapsed');
      }

      functions.buttonClicked(event, $ths);

    },

    /**
     * Toggles an Open Nested button.
     */
    toggleNested: function (event) {

      let $ths = $(this);
      event.preventDefault();

      let $table = $ths.closest('table');
      let $belowButtons = $table.find('.paragroup.toggle-below');
      let $nestedButtons = $table.find('.paragroup.toggle-nested');

      if($ths.hasClass('collapsed')) {
        functions.setNestedExpanded($nestedButtons);
        $belowButtons.each(functions.expandBelowBulk);
      }
      else if($ths.hasClass('expanded')) {
        functions.resetAllNested($ths);
      }

      functions.buttonClicked(event, $ths);

    },

    /**
     * Toggles an Open All Paragraphs button.
     */
    toggleAll: function (event) {

      let $ths = $(this);
      event.preventDefault();

      let $belowButtons = $('.paragroup.toggle-below');
      let $nestedButtons = $('.paragroup.toggle-nested');

      if($ths.hasClass('collapsed')) {

        $ths.val('Close All Paragraphs');
        $ths.removeClass('collapsed').addClass('expanded');
        $belowButtons.each(functions.expandBelowBulk);
        functions.setNestedExpanded($nestedButtons);

      }
      else if($ths.hasClass('expanded')) {

        $ths.val('Open All Paragraphs');
        $ths.removeClass('expanded').addClass('collapsed');
        $belowButtons.each(functions.collapseBelowBulk);
        functions.setNestedCollapsed($nestedButtons);

      }

    },

    /**
     * Get HTML for Open All Paragraphs button.
     */
    allButtonHtml: function () {

      let value = "'Open All Paragraphs'";
      let classes = "'button button--primary paragroup toggle-all collapsed'";

      let tooltip =
        "'This button opens / closes all the nested items " +
        "for all the paragraphs on this page.'";

      let allButton =
        "<input class=" + classes + " title=" + tooltip +
        " value=" + value + " readonly>";

      return allButton;

    },

    /**
     * Append Open All Paragraphs button to page.
     */
    appendAllButton: function () {

      let editParagroupSel = '#edit-group-paragraphs';
      let $editParagroup = $(editParagroupSel, context);
      let regionSel, $regionEl;

      if($editParagroup.length) {
        $regionEl = $editParagroup;
      }
      else {
        regionSel = '.layout-region-node-main';
        $regionEl = $(regionSel, context);
      }

      if(!$editParagroup.length && !$regionEl.length) {
        regionSel = '.layout-region--node-main';
        $regionEl = $(regionSel, context);
      }

      let tableSel = 'table.field-multiple-table';

      if($regionEl.find(tableSel).length) {

        let allButton = functions.allButtonHtml();
        let $buttonAppended = $regionEl.find('.paragroup.toggle-all').length;

        if(!$buttonAppended) {
          $regionEl.prepend(allButton);
        }

      }

    },

    /**
     * Get paragraph selector and elements.
     */
    getParagraphEls: function () {

      let paragraphSel =
        '.field--widget-paragraph-group-details-widget ' +
        '> div > div > table > thead > tr > th.field-label';

      return $(paragraphSel, context);

    },

    /**
     * Get HTML for Open Below button.
     */
    belowButtonHtml: function () {

      let value = "'Open Below'";
      let classes = "'button button--small paragroup toggle-below collapsed'";
      let tooltip = "'This button opens / closes the items immediately below.'";

      let input =
        "<input class=" + classes + " title=" + tooltip +
        " value=" + value + " readonly>";

      let belowButton =
        "<div class='paragroup button-wrapper'>" +
          "<span class='buttons'>" +
             "<a class='paragroup scrollto'></a>" + input +
          "</span>" +
        "</div>";

      return belowButton;

    },

    /**
     * Append Open Below button to page.
     */
    appendBelowButton: function () {

      let $paragraphEls = functions.getParagraphEls();
      let belowButton = functions.belowButtonHtml();

      $paragraphEls.each(function () {

        let $ths = $(this);
        let $belowAppended = $ths.find('.paragroup.button-wrapper').length;

        if(!$belowAppended) {
          $ths.append(belowButton);
        }

      });

    },

    /**
     * Get HTML for Open Nested button.
     */
    nestedButtonHtml: function () {

      let value = "'Open Nested'";
      let classes = "'button button--small paragroup toggle-nested collapsed'";

      let tooltip =
        "'This button opens / closes all the nested items in this paragraph.'";

      let nestedButton =
        "<input class=" + classes + " title=" + tooltip +
        " value=" + value + " readonly>";

      return nestedButton;

    },

    /**
     * Append Open Nested button to page.
     */
    appendNestedButton: function () {

      let $paragraphEls = functions.getParagraphEls();
      let nestedButton = functions.nestedButtonHtml();

      $paragraphEls.each(function () {

        let $ths = $(this);
        let $nestedAppended = $ths.find('.paragroup.toggle-nested').length;
        let hasSubParagraphs = functions.paragraphHasSubParagraphs($ths);

        if(!$nestedAppended && hasSubParagraphs) {

          $ths
            .find('.paragroup.button-wrapper > .buttons')
            .append(nestedButton);

        }

      });

    },

    /**
     * Adds class to html element if not already added
     */
    addHtmlClass: function (className) {

      let $html = $('html');

      if(!$html.hasClass(className)) {
        $html.addClass(className);
      }

    },

    /**
     * Get the Drupal version number passed in the settings value,
     * and return a class name for this version.
     */
    getVerClass: function (settings) {

      let version = parseInt(settings.paragraph_group_ver);
      let verClass;

      switch(version) {

        case 8:
          verClass = 'pg-d-eight';
          break;

        case 9:
          verClass = 'pg-d-nine';
          break;

        case 10:
          verClass = 'pg-d-ten';
          break;

      }

      return verClass;

    },

    /**
     * Adds theme modifications config from module config page to HTML element
     * so that CSS styling is updated accordingly.
     */
    themeMods: function (settings) {

      let verClass = functions.getVerClass(settings);

      let configs = {
        'paragraph_group_sc': 'pg-sidebar-config',
        'paragraph_group_fwf': 'pg-full-width-forms',
        'paragraph_group_ver': verClass
      };

      $.each(configs, function (key, val) {

        if(settings.hasOwnProperty(key)) {
          functions.addHtmlClass(val);
        }

      });

    },

    /**
     * When a user clicks 'remove' in a paragraph, the Paragraphs 'Confirm
     * Removal' button is added via AJAX, and the Detail is loaded in the
     * closed state. This function ensures they are opened.
     */
    confirmRemoval: function () {

      let confirmRemovalSel =
        '.paragraphs-dropbutton-wrapper .confirm-remove';

      let $confirmRemovalEl = $(confirmRemovalSel, context);

      if($confirmRemovalEl.length) {

        let detailsSel = 'details.js-form-wrapper:not(.field-group-tab)';
        let $details = $confirmRemovalEl.parents(detailsSel);

        $details.prop('open', true);

      }

    },

    /**
     * New Paragraphs are added via AJAX. This function ensures all Details
     * within new Paragraphs content added in this way are open.
     */
    addedParagraph: function () {

      let newContentSel = '.draggable > td > .ajax-new-content';
      let $newContent = $(newContentSel, context);

      if($newContent.length) {

        let $details = $newContent.find('> div > details');
        $details.prop('open', true);

      }

    },

    /**
     * Appends the 'Return to top' to the Edit page actions at the bottom
     * of the edit page.
     */
    appendReturnToTop: function () {

      let elementSel = '#edit-actions';
      let $elementEl = $(elementSel);

      let elementHtml =
        "<button class='link paragroup return-to-top'>Return to top</button>";

      if(!$('html .paragroup.return-to-top').length) {
        $elementEl.append(elementHtml);
      }

      $('html .paragroup.return-to-top').click(function () {
        event.preventDefault();
        $(window).scrollTop(0);
      });

    },

    /**
     * Initialisation function.
     */
    init: function (settings) {

      functions.addHtmlClass('paragroup-on');
      functions.themeMods(settings);
      functions.confirmRemoval();
      functions.addedParagraph();

    },

    /**
     * New content added via AJAX leaves an .ajax-new-content div. Lots
     * of these can become nested if buttons are clicked multiple times. This
     * function removes these divs.
     */
    removeAjaxNewContentDiv: function () {

      let ajaxNewContentSel =
        '.field--widget-paragraph-group-details-widget .ajax-new-content > *';

      $(ajaxNewContentSel).unwrap();

    },

    /**
     * Appends all buttons in this file to the edit page.
     */
    appendButtons: function () {

      functions.removeAjaxNewContentDiv();
      functions.appendAllButton();
      functions.appendBelowButton();
      functions.appendNestedButton();
      functions.appendReturnToTop();

    },

    /**
     * Adds the events for all the buttons, and ensures events are only
     * applied once for the case of new content added via AJAX.
     */
    addToggleButtonEvents: function () {

      let tea = 'toggle-event-added';

      let events = {
        '.toggle-all': functions.toggleAll,
        '.toggle-below': functions.toggleBelow,
        '.toggle-nested': functions.toggleNested
      };

      // Below registers events and prevents them
      // being registered multiple times.
      $.each(events, function (key, val) {

        let $button = $(key, context);

        $button.each(function () {

          let $ths = $(this);

          if(!$ths.hasClass(tea)) {
            $ths.click(val).addClass(tea);
          }

        });

      });

    },

    /**
     * Main function containing all functionality in this file.
     */
    main: function (settings) {

      functions.init(settings);
      functions.appendButtons();
      functions.addToggleButtonEvents();

    },

  };

  functions.main(settings);

};


/*global Drupal*/
/*global jQuery*/
/*global drupalSettings*/
(function ($, drupalSettings) {

  Drupal.behaviors.paragraph_group = {

    attach: function attach(context, settings) {

      if(settings.hasOwnProperty('paragraph_group_details_widget')) {
        paragroup($, context, settings);
      }

    }

  };

})(jQuery, drupalSettings);
