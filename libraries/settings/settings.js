function paragroup($, context) {

  let functions = {

    /**
     * Open a Details element.
     */
    openDetails: function (id) {

      let $detailsEl = $(id);

      if(!$detailsEl.attr('open')) {
        $detailsEl.attr('open', '');
      }

    },

    /**
     * Clicks the select-all input for a configuration section.
     */
    clickSelectAll: function (sectionId, deselect = false) {

      let $selectAll = $(sectionId + ' th.select-all input');
      let title = $selectAll.attr('title');
      let titleComparison = null;

      if(!deselect) {
        titleComparison = 'Select all rows in this table';
      }
      else {
        titleComparison = 'Deselect all rows in this table';
      }

      if(title == titleComparison) {
        $selectAll.click();
      }

    },

    /**
     * Set up main sections checkboxes.
     */
    mainSections: function () {

      let sections = [
        '#edit-details-widget-boxes',
        '#edit-admin-titles-boxes',
        '#edit-field-groups-boxes'
      ];

      $.each(sections, function (index, id) {
        functions.clickSelectAll(id);
        functions.openDetails(id);
      });

    },

    /**
     * Set up theme mods checkboxes.
     */
    themeMods: function () {

      let themeModsId = '#edit-theme-mods-boxes';

      functions.clickSelectAll(themeModsId, true);

      $('#edit-theme-mods-boxes-full-width-forms').prop('checked', true);
      $('#edit-theme-mods-boxes-sidebar-config').removeAttr('disabled').prop('checked', false);

      functions.openDetails(themeModsId);

    },

    /**
     * When "clicking here" link is clicked, sets up recommended configuration.
     */
    recommendedConfig: function () {

      $('#pg-rec-config-link').click(function (event) {

        event.preventDefault();
        functions.mainSections();
        functions.themeMods();

      });

    },

    /**
     * Action for disabling / preventing clicks on sidebar config checkbox,
     * used in function below this one.
     */
    scAction: function () {

      let $sc = $('#edit-theme-mods-boxes-sidebar-config');
      let $fwf = $('#edit-theme-mods-boxes-full-width-forms');

      if($fwf.is(':checked')) {
        $sc.removeAttr("disabled");
      }
      else {
        $sc.prop('checked', false).attr("disabled", true);
      }

    },

    /**
     * Prevents 'Move page configuration sidebar to bottom of edit pages'
     * config checkbox from being checked, without 'Full Width Claro Forms'
     * checkbox being checked first.
     */
    sidebarConfig: function () {

      functions.scAction();
      $('#edit-theme-mods-boxes-full-width-forms').change(functions.scAction);

    },

    /**
     * Adds class to html element confirming module has completed initialising.
     */
    addClassToHtml: function (className) {

      let $html = $('html');

      if(!$html.hasClass(className)) {
        $html.addClass(className);
      }

    },

    /**
     * Main function containing all functionality in this file.
     */
    main: function () {

      let $sa = $('#edit-theme-mods-boxes th.select-all input');

      if($sa.length && !$('html').hasClass('pg-settings-complete')) {
        functions.recommendedConfig();
        functions.sidebarConfig();
        functions.addClassToHtml('pg-settings-complete');
      }

    },

  };

  functions.main();

};


/*global Drupal*/
/*global jQuery*/
/*global drupalSettings*/
(function ($, drupalSettings) {

  Drupal.behaviors.paragraph_group = {

    attach: function attach(context, settings) {

      if(settings.hasOwnProperty('paragraph_group_settings')) {
        paragroup($, context);
      }

    }

  };

})(jQuery, drupalSettings);
