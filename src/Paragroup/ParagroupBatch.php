<?php

namespace Drupal\paragraph_group\Paragroup;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class ParagroupBatch.
 *
 * @package Drupal\paragraph_group\Paragroup
 *
 * Contains all functions used by the Drupal Batch API to create and delete
 * Paragraph Details widgets, Administrative Title fields and field groups.
 */
class ParagroupBatch {

  use StringTranslationTrait;

  protected $stringTranslation;
  protected $formData;

  /**
   * Constructor.
   */
  public function __construct(
    TranslationInterface $string_translation,
    ParagroupFormData $form_data
  ) {

    $this->stringTranslation = $string_translation;
    $this->formData = $form_data;

  }

  /**
   * Create function for constructor.
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('string_translation'),
      $container->get('paragraph_group.form_data')
    );

  }

  /**
   * General actions required for adding or removing a Paragraph Details
   * widget to / from a field.
   */
  private static function addRemoveDetailsWidgetAction($field_id, $type) {

    [$entity_type, $bundle, $field_name] = explode('.', $field_id);

    $field = \Drupal::service('entity_display.repository')
      ->getFormDisplay($entity_type, $bundle)
      ->getComponent($field_name);

    $field['type'] = $type;
    $field['settings']['edit_mode'] = 'open';

    $updated = \Drupal::service('entity_display.repository')
      ->getFormDisplay($entity_type, $bundle)
      ->setComponent($field_name, $field)
      ->save();

    return $updated;

  }

  /**
   * Array data function containing function info for each settings
   * form section.
   */
  private function batchOpsData() {

    $data = [

      'admin_titles_section' => [
        'prefix' => '\Drupal\paragraph_group\Paragroup\ParagroupBatch::',
        'add' => 'createAdminTitleField',
        'remove' => 'deleteAdminTitleField',
      ],

      'details_widget_section' => [
        'prefix' => '\Drupal\paragraph_group\Paragroup\ParagroupBatch::',
        'add' => 'addDetailsWidget',
        'remove' => 'removeDetailsWidget',
      ],

      'field_groups_section' => [
        'prefix' => '\Drupal\paragraph_group\Paragroup\ParagroupBatch::',
        'add' => 'createFieldGroups',
        'remove' => 'deleteFieldGroups',
      ],

    ];

    return $data;

  }

  /**
   * Helper function returning add or remove based on $diff_val generated from
   * analysis of old and new checkbox form state.
   */
  private function addRemove($diff_val) {

    if ($diff_val) {
      return 'add';
    }

    return 'remove';

  }

  /**
   * Settings form uses machine name of fields without dots for checkboxes.
   * This function gets the corresponding machine name with the dots.
   */
  private function getFieldConfigWithDots($query_key) {

    $field_config_ids = $this->formData->getFieldConfig();
    $processed_ids = [];

    foreach ($field_config_ids as $key => $val) {
      $key = str_replace('.', '', $key);
      $processed_ids[$key] = $val;
    }

    if (isset($processed_ids[$query_key])) {
      return $processed_ids[$query_key];
    }

    return FALSE;

  }

  /**
   * Gets batch operations for each settings form section.
   */
  private function getAdditionalBatchOps($section, $boxes_diff) {

    $ops = [];
    $data = $this->batchOpsData();

    foreach ($boxes_diff as $id => $diff_val) {

      if ($section == 'details_widget_section') {
        $id = $this->getFieldConfigWithDots($id);
      }

      $add_remove = $this->addRemove($diff_val);
      $prefix = $data[$section]['prefix'];
      $func = $data[$section][$add_remove];

      $ops[] = [$prefix . $func, [$id]];

    }

    return $ops;

  }

  /**
   * Returns initial base array for Batch API.
   */
  private function getBatchBaseData() {

    $finished =
      '\Drupal\paragraph_group\Paragroup\ParagroupBatch::finished';

    $batch = [
      'title' => $this->t('Updating Paragraph Group module settings'),
      'operations' => [],
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing'),
      'finished' => $finished,
    ];

    return $batch;

  }

  /**
   * Analyses checkboxes from the old and new form states, and returns the
   * differences for boxes which have been changed.
   */
  private function getBoxesDiff($old_boxes, $new_boxes) {

    $diff = [];

    foreach ($new_boxes as $key => $new_val) {

      if ($key != 'select_all') {

        if (isset($old_boxes[$key])) {

          $old_val = $old_boxes[$key];

          if ($old_val != $new_val) {

            $diff_val = FALSE;

            if (!$old_val && $new_val) {
              $diff_val = TRUE;
            }

            $diff[$key] = $diff_val;

          }

        }
        elseif ($new_val) {
          $diff[$key] = TRUE;
        }

      }

    }

    return $diff;

  }

  /**
   * Array data function returning message info for each function name.
   */
  private static function getSectionInfoFromFuncName($func_name) {

    $func_names = [
      'createAdminTitleField' => ['Administrative Title Fields', 'created'],
      'deleteAdminTitleField' => ['Administrative Title Fields', 'deleted'],
      'addDetailsWidget' => ['Paragraph Details Widgets', 'added'],
      'removeDetailsWidget' => ['Paragraph Details Widgets', 'removed'],
      'createFieldGroups' => ['Field Group sets', 'added'],
      'deleteFieldGroups' => ['Field Group sets', 'deleted'],
    ];

    return $func_names[$func_name];

  }

  /**
   * Finished function used by Batch API.
   */
  public static function finished($success, $results, $operations) {

    if ($success && empty($operations)) {

      $results = $results['paragroup'];

      foreach ($results as $func_name => $result) {

        if ($result) {

          $result = $result . ' out of ' . $result;
          $info = self::getSectionInfoFromFuncName($func_name);
          $msg = $result . ' ' . $info[0] . ' ' . $info[1] . '.';

          \Drupal::messenger()->addStatus(t($msg));

        }

      }

    }
    else {

      $msg = t('An error occured while processing the selected items.');
      \Drupal::messenger()->addStatus($msg);

    }

  }

  /**
   * Updates the context variable used by Batch API during processing by
   * keeping track of the number of times each function has been run.
   */
  private static function updateContext(&$context, $func_name) {

    if (!isset($context['results']['paragroup'])) {

      $context['results']['paragroup'] = [
        'createAdminTitleField' => 0,
        'deleteAdminTitleField' => 0,
        'addDetailsWidget' => 0,
        'removeDetailsWidget' => 0,
        'createFieldGroups' => 0,
        'deleteFieldGroups' => 0,
      ];

    }

    if (array_key_exists($func_name, $context['results']['paragroup'])) {
      $context['results']['paragroup'][$func_name]++;
    }

  }

  /**
   * Function used by Batch API to create Administrative Title fields.
   */
  public static function createAdminTitleField($bundle, &$context) {

    $field_created =
      ParagroupFieldManager::createParagraphAdminTitleField($bundle);

    if ($field_created) {
      self::updateContext($context, 'createAdminTitleField');
    }

    return $field_created;

  }

  /**
   * Function used by Batch API to delete Administrative Title fields.
   */
  public static function deleteAdminTitleField($bundle, &$context) {

    $field_deleted =
      ParagroupFieldManager::deleteParagraphAdminTitleField($bundle);

    if ($field_deleted) {
      self::updateContext($context, 'deleteAdminTitleField');
    }

    return $field_deleted;

  }

  /**
   * General function used by Batch API to add / remove a Paragraph Details
   * widget to a field.
   */
  private static function opDetailsWidget($field_id, $type, &$context, $op) {

    $details_widget_opped =
      self::addRemoveDetailsWidgetAction($field_id, $type);

    if ($details_widget_opped) {
      self::updateContext($context, $op);
    }

    return $details_widget_opped;

  }

  /**
   * Function used by Batch API to add a Paragraph Details widget to a field.
   */
  public static function addDetailsWidget($field_id, &$context) {

    $op = 'addDetailsWidget';
    $type = 'paragraph_group_details_widget';

    $details_widget_added =
      self::opDetailsWidget($field_id, $type, $context, $op);

    return $details_widget_added;

  }

  /**
   * Function used by Batch API to remove a Paragraph Details widget
   * from a field.
   */
  public static function removeDetailsWidget($field_id, &$context) {

    $op = 'removeDetailsWidget';

    /* Consider setting this to the now default Paragraphs (stable)
    AJAX widget, formerly called Paragraphs Experimental widget, now with
    machine name 'paragraphs', as shown in comment below: */
    // $type = 'paragraphs';
    $type = 'entity_reference_paragraphs';

    $details_widget_removed =
      self::opDetailsWidget($field_id, $type, $context, $op);

    return $details_widget_removed;

  }

  /**
   * Function used by Batch API to create Field Groups.
   */
  public static function createFieldGroups($bundle, &$context) {

    $created_groups = ParagroupFieldGroupManager::createFieldGroups($bundle);

    if ($created_groups) {
      self::updateContext($context, 'createFieldGroups');
    }

    return $created_groups;

  }

  /**
   * Function used by Batch API to delete Field Groups.
   */
  public static function deleteFieldGroups($bundle, &$context) {

    $deleted_groups = ParagroupFieldGroupManager::deleteFieldGroups($bundle);

    if ($deleted_groups) {
      self::updateContext($context, 'deleteFieldGroups');
    }

    return $deleted_groups;

  }

  /**
   * Returns the final array of data instrucing the Batch API on what to do.
   */
  public function getBatchData($old_and_new_config) {

    $form_structure = $this->formData->getFormStructure();
    $batch = $this->getBatchBaseData();

    foreach ($form_structure as $section => $boxes) {

      if ($section != 'theme_mods_section') {

        $boxes_name = 'paragraph_group.' . $boxes;
        $old_boxes = $old_and_new_config[0][$boxes_name];
        $new_boxes = $old_and_new_config[1][$boxes_name];
        $boxes_diff = $this->getBoxesDiff($old_boxes, $new_boxes);

        if (!empty($boxes_diff)) {
          $ops = $this->getAdditionalBatchOps($section, $boxes_diff);
          $batch['operations'] = array_merge($batch['operations'], $ops);
        }

      }

    }

    return $batch;

  }

}
