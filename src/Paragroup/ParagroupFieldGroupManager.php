<?php

namespace Drupal\paragraph_group\Paragroup;

/**
 * Class ParagroupFieldGroupManager.
 *
 * @package Drupal\paragraph_group\Paragroup
 *
 * Creates, deletes and manages Field Groups
 */
class ParagroupFieldGroupManager {

  /**
   * General function to create Field Groups, interfacing with the Field Group
   * module function for doing so.
   */
  private static function createFieldGroup($data) {

    $new_group = (object) [
      'mode' => 'default',
      'context' => 'form',
      'children' => $data['children'],
      'format_type' => $data['format_type'],
      'group_name' => $data['group_name'],
      'entity_type' => $data['entity_type'],
      'bundle' => $data['bundle'],
      'parent_name' => $data['parent_name'],
      'weight' => $data['weight'],
      'region' => $data['region'],
      'format_settings' => $data['format_settings'],
      'label' => $data['label'],
    ];

    field_group_group_save($new_group);

    return TRUE;

  }

  /**
   * Creates the main tabs container containing each of the Field Groups.
   */
  private static function createTabsGroup($schema, $bundle) {

    $format_settings = [
      'show_empty_fields' => 0,
      'id' => '',
      'classes' => '',
      'direction' => 'vertical',
      'width_breakpoint' => '640',
    ];

    $data = [
      'group_name' => 'group_tabs',
      'entity_type' => 'node',
      'bundle' => $bundle,
      'weight' => 0,
      'label' => 'Tabs',
      'region' => 'content',
      'parent_name' => '',
      'format_type' => 'tabs',
      'format_settings' => $format_settings,
      'children' => $schema,
    ];

    self::createFieldGroup($data);

    return TRUE;

  }

  /**
   * Saves the data required to create each Field Group under Tabs.
   */
  private static function saveTabData($data) {

    $format_settings = [
      'show_empty_fields' => 0,
      'id' => '',
      'classes' => '',
      'description' => '',
      'formatter' => 'closed',
      'required_fields' => 1,
    ];

    $general_data = [
      'entity_type' => 'node',
      'region' => 'content',
      'parent_name' => 'group_tabs',
      'format_type' => 'tab',
      'format_settings' => $format_settings,
      'children' => [],
    ];

    $data = array_merge($data, $general_data);
    self::createFieldGroup($data);

    return TRUE;

  }

  /**
   * Adds fields to the field group.
   */
  private static function populateGroup($group_obj, $children) {

    $group_obj->children = $children;
    field_group_group_save($group_obj);

    return TRUE;

  }

  /**
   * Gets a list of fields for a bundle, as generated on the Manage Fields
   * page for a bundle.
   */
  private static function getFieldList($bundle) {

    $field_info = \Drupal::service('entity_type.manager')
      ->getListBuilder('field_config')
      ->render('node', $bundle);

    $fields = array_keys($field_info['table']['#rows']);

    foreach ($fields as $i => $field) {

      [$entity_type, $bundle, $field_name] = explode('.', $field);
      $fields[$i] = $field_name;

    }

    $fields = array_merge(['title'], $fields);

    return $fields;

  }

  /**
   * Filters fields and adds full machine name based on entity type (node)
   * and bundle.
   */
  private static function getFieldData($fields, $bundle) {

    $field_data = $fields;
    $field_names = ['title', 'body'];

    foreach ($field_names as $field_name) {
      if (($key = array_search($field_name, $field_data)) !== FALSE) {
        unset($field_data[$key]);
      }
    }

    foreach ($field_data as $i => $field) {
      $field = 'node.' . $bundle . '.' . $field;
      $field_data[$i] = $field;
    }

    return $field_data;

  }

  /**
   * Gets list of field types with no UI.
   */
  private static function filterFieldList() {

    $defs =
      \Drupal::service('plugin.manager.field.field_type')
        ->getDefinitions();

    foreach ($defs as $name => $info) {

      if (!$info['no_ui']) {
        unset($defs[$name]);
      }

    }

    return array_keys($defs);

  }

  /**
   * Filters fields from field_info which have no UI.
   */
  private static function filterFieldsInfo($field_info) {

    $filter_list = self::filterFieldList();

    foreach ($field_info['field_config'] as $machine_name => $field_config) {

      $field_type = $field_config->get('field_type');
      $in_filter_list = in_array($field_type, $filter_list);

      if ($in_filter_list) {

        [$entity_type, $bundle, $field_name] =
          explode('.', $machine_name);

        $field_key = array_search($field_name, $field_info['fields']);

        unset($field_info['fields'][$field_key]);
        unset($field_info['field_config'][$machine_name]);

      }

    }

    return $field_info;

  }

  /**
   * Gets info about fields for the analyseContentType function.
   */
  private static function getFieldsInfo($bundle) {

    $fields = self::getFieldList($bundle);
    $field_data = self::getFieldData($fields, $bundle);

    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->loadMultiple($field_data);

    $field_info = [
      'fields' => $fields,
      'field_config' => $field_config,
    ];

    $filtered_field_info = self::filterFieldsInfo($field_info);

    return $filtered_field_info;

  }

  /**
   * Gets the type of a field. Returns $field_type if the field isn't an
   * entity reference; otherwise it retrieves the entity reference type
   * from the field settings.
   */
  private static function getType($field_name, $fields_info, $bundle) {

    $field_id = 'node.' . $bundle . '.' . $field_name;

    if (isset($fields_info['field_config'][$field_id])) { 

      $field_config = $fields_info['field_config'][$field_id];
      $field_type = $field_config->get('field_type');

      if ($field_type != 'entity_reference' &&
         $field_type != 'entity_reference_revisions') {
        return $field_type;
      }

      $settings = $field_config->getSettings();

      if (isset($settings['target_type'])) {
        return $settings['target_type'];
      }

    }

    return FALSE;

  }

  /**
   * Updates the type $info array based on the $type variable.
   */
  private static function setTypeInfo($info, $type, $field_name) {

    if ($type) {

      if ($type == 'paragraph') {
        $info['has-paragraphs'] = TRUE;
      }
      elseif ($type == 'media' || $type == 'file') {
        $info['has-media-uploads'] = TRUE;
      }
      else {
        $info['has-misc'] = TRUE;
      }

    }
    elseif ($field_name != 'title') {
      $info['has-misc'] = TRUE;
    }

    return $info;

  }

  /**
   * Analyses a content type bundle, and returns an array noting the
   * various kinds of fields contained by it.
   */
  private static function analyseContentType($bundle) {

    $fields_info = self::getFieldsInfo($bundle);

    if (count($fields_info['fields']) > 2) {

      $info = [
        'has-body' => FALSE,
        'has-paragraphs' => FALSE,
        'has-media-uploads' => FALSE,
        'has-misc' => FALSE,
      ];

      $filter_list = self::filterFieldList();

      foreach ($fields_info['fields'] as $field_name) {

        if ($field_name == 'body') {
          $info['has-body'] = TRUE;
        }
        else {

          $type = self::getType(
            $field_name, $fields_info, $bundle
          );

          if (!in_array($type, $filter_list)) {
            $info = self::setTypeInfo($info, $type, $field_name);
          }

        }

      }

      return $info;

    }

    return FALSE;

  }

  /**
   * Array data function mapping the bundle info onto the Field Group schema
   * of field groups.
   */
  private static function getSchemaData() {

    $schema = [
      'group_main' => TRUE,
      'group_paragraphs' => TRUE,
      'group_media' => TRUE,
      'group_misc' => TRUE,
    ];

    $info_to_schema = [
      'has-body' => 'group_misc',
      'has-paragraphs' => 'group_paragraphs',
      'has-media-uploads' => 'group_media',
      'has-misc' => 'group_misc',
    ];

    $data = [
      'schema' => $schema,
      'info_to_schema' => $info_to_schema,
    ];

    return $data;

  }

  /**
   * Schema array is initailly returned with all keys set to true; this
   * function sets them to false if the bundle info indicates the bundle has
   * no fields for that group.
   */
  private static function getFieldGroupSchema($field_info) {

    $data = self::getSchemaData();
    $schema = $data['schema'];

    foreach ($data['info_to_schema'] as $info_item => $schema_item) {

      if (!$field_info[$info_item]) {
        $schema[$schema_item] = FALSE;
      }

    }

    $schema = array_keys(array_filter($schema));

    if (count($schema) > 1) {
      return $schema;
    }

    return FALSE;

  }

  /**
   * Returns the custom data required for each of the main Field Group tabs.
   */
  private static function getGroupData($bundle) {

    $data = [

      'group_main' => [
        'group_name' => 'group_main',
        'label' => 'Main',
      ],

      'group_paragraphs' => [
        'group_name' => 'group_paragraphs',
        'label' => 'Paragraphs',
      ],

      'group_media' => [
        'group_name' => 'group_media',
        'label' => 'Media Uploads',
      ],

      'group_misc' => [
        'group_name' => 'group_misc',
        'label' => 'Miscellaneous',
      ],

    ];

    return $data;

  }

  /**
   * Creates all the field group tabs including the main tab container.
   */
  private static function createGroups($schema, $bundle) {

    self::createTabsGroup($schema, $bundle);
    $data = self::getGroupData($bundle);
    $i = 1;

    foreach ($schema as $group_name) {

      $group_data = $data[$group_name];

      $group_data['weight'] = $i;
      $group_data['bundle'] = $bundle;

      self::saveTabData($group_data);

      $i++;

    }

    return TRUE;

  }

  /**
   * Deletes all field groups in a bundle.
   */
  private static function deleteAllGroupsInBundle($entity_type, $bundle) {

    $groups =
      field_group_info_groups($entity_type, $bundle, 'form', 'default');

    foreach ($groups as $group_obj) {
      field_group_delete_field_group($group_obj);
    }

    return TRUE;

  }

  /**
   * Prepares a content type for new field groups by deleting its previous
   * ones, and adjusting the field weights so fields can be properly added
   * to new groups.
   */
  private static function prepareContentType($schema, $bundle) {

    $count = count($schema) + 1;

    self::deleteAllGroupsInBundle('node', $bundle);
    ParagroupFieldManager::prepareFieldWeights('node', $bundle, $count);

    return TRUE;

  }

  /**
   * Gets which group a field should be added to.
   */
  private static function getGroupOfField(
    $bundle,
    $field_name,
    $schema,
    $fields_info
  ) {

    if ($field_name == 'title' || $field_name == 'body') {
      return 'group_main';
    }

    $type = self::getType(
      $field_name, $fields_info, $bundle
    );

    if ($type) {

      if ($type == 'paragraph') {
        return 'group_paragraphs';
      }

      if ($type == 'media' || $type == 'file') {
        return 'group_media';
      }

    }

    if (in_array('group_misc', $schema)) {
      return 'group_misc';
    }

    return 'group_main';

  }

  /**
   * Returns an array noting which group each field should be added to.
   */
  private static function analyseFields($schema, $bundle) {

    $fields_info = self::getFieldsInfo($bundle);
    $fields_to_groups = [];

    foreach ($fields_info['fields'] as $field_name) {

      $fields_to_groups[$field_name] =
        self::getGroupOfField(
          $bundle, $field_name, $schema, $fields_info
        );

    }

    return $fields_to_groups;

  }

  /**
   * Gets the list of children fields for each group.
   */
  private static function getChildren($schema, $fields_to_groups) {

    $children = [];

    foreach ($schema as $group_name) {
      $children[$group_name] = [];
    }

    foreach ($fields_to_groups as $field => $group) {
      $children[$group][] = $field;
    }

    return $children;

  }

  /**
   * Adds children fields to each group.
   */
  private static function populateGroups($schema, $bundle) {

    $fields_to_groups = self::analyseFields(
      $schema, $bundle
    );

    $children = self::getChildren(
      $schema, $fields_to_groups
    );

    $groups = field_group_info_groups('node', $bundle, 'form', 'default');

    foreach ($children as $group_name => $children_set) {

      self::populateGroup(
        $groups[$group_name], $children_set
      );

    }

    return TRUE;

  }

  /**
   * Analyses bundle and returns whether or not it is currently suitable
   * for adding field groups.
   */
  public static function proceedWithCreate($bundle) {

    $field_info = self::analyseContentType($bundle);

    if ($field_info) {

      $schema = self::getFieldGroupSchema($field_info);

      if ($schema) {
        return $schema;
      }

    }

    return FALSE;

  }

  /**
   * Main public function called by ParagroupBatch to create the field groups
   * for a particular bundle.
   */
  public static function createFieldGroups($bundle) {

    $schema = self::proceedWithCreate($bundle);

    if ($schema) {

      self::prepareContentType($schema, $bundle);
      self::createGroups($schema, $bundle);
      self::populateGroups($schema, $bundle);

      return TRUE;

    }

    return FALSE;

  }

  /**
   * Main public function called by ParagroupBatch to delete the field groups
   * for a particular bundle.
   */
  public static function deleteFieldGroups($bundle) {

    $deleted_groups = self::deleteAllGroupsInBundle('node', $bundle);

    return $deleted_groups;

  }

}
