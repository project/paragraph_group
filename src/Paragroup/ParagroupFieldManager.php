<?php

namespace Drupal\paragraph_group\Paragroup;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Class ParagroupFieldManager.
 *
 * @package Drupal\paragraph_group\Paragroup
 *
 * Manages Drupal fields for creating and deleting Administrative Title
 * fields. Contains in particular code for adjusting field weights.
 */
class ParagroupFieldManager {

  /**
   * Gets the field name of the lowest weight field from bundle fields.
   */
  private static function getLowestWeight($fields) {

    $lowest_weight = FALSE;
    $lw_field_name = FALSE;

    foreach ($fields as $field_name => $field) {

      if ($lowest_weight === FALSE ||
         $field['weight'] < $lowest_weight
      ) {
        $lowest_weight = $field['weight'];
        $lw_field_name = $field_name;
      }

    }

    return $lw_field_name;

  }

  /**
   * Filters empty keys from fields.
   */
  private static function filterFields($fields) {

    if (isset($fields[""])) {
      unset($fields[""]);
    }

    return $fields;

  }

  /**
   * Takes a list of fields and returns an array with the fields reordered
   * by their current weight.
   */
  private static function orderFieldsByWeight($fields) {

    $fields = self::filterFields($fields);
    $fields_by_weight = [];

    while (!empty($fields)) {

      $lw_field_name = self::getLowestWeight($fields);
      $field = $fields[$lw_field_name];

      $field['field_name'] = $lw_field_name;
      $fields_by_weight[] = $field;
      unset($fields[$lw_field_name]);

    }

    return $fields_by_weight;

  }

  /**
   * Adjusts field weights to a discrete integer setting.
   * E.g. -1, 4, 100, ... becomes 1, 2, 3, ... if the
   * $start_weight is 1.
   */
  private static function orderFieldsByRationalWeight(
    $fields_by_weight,
    $start_weight
  ) {

    $i = $start_weight;
    $fields_by_rational_weight = [];

    foreach ($fields_by_weight as $field) {

      $field['weight'] = $i;
      $fields_by_rational_weight[$i] = $field;
      $i++;

    }

    return $fields_by_rational_weight;

  }

  /**
   * Returns the field array elements to their original state with
   * updated field weights, so that the fields are readable by the Drupal
   * field update function.
   */
  private static function resetFieldsWithNewWeights(
    $fields_by_rational_weight
  ) {

    $fields = [];

    foreach ($fields_by_rational_weight as $field) {

      $field_name = $field['field_name'];
      unset($field['field_name']);
      $fields[$field_name] = $field;

    }

    return $fields;

  }

  /**
   * Updates each field containing new weight.
   */
  private static function updateFieldWeights($entity_type, $bundle, $fields) {

    foreach ($fields as $field_name => $field) {

      \Drupal::service('entity_display.repository')
        ->getFormDisplay($entity_type, $bundle)
        ->setComponent($field_name, $field)
        ->save();

    }

    return TRUE;

  }

  /**
   * Carries out the actions to create a new field, using Drupal's
   * FieldConfig and FieldStorageConfig classes.
   */
  private static function createFieldActionsConfig(
    $field_storage_config,
    $field_config
  ) {

    $bundle = $field_config['bundle'];
    $field_name = $field_config['field_name'];
    $entity_type = $field_config['entity_type'];

    $load_field_storage_config =
      FieldStorageConfig::loadByName($entity_type, $field_name);

    if (!$load_field_storage_config) {
      FieldStorageConfig::create($field_storage_config)->save();
    }

    $load_field_config =
      FieldConfig::loadByName($entity_type, $bundle, $field_name);

    $field_config_created = FALSE;

    if (!$load_field_config) {

      FieldConfig::create($field_config)->save();
      $field_config_created = TRUE;

    }

    return $field_config_created;

  }

  /**
   * Main actions to create field are carried out by createFieldActionsConfig;
   * in addition to that, this function updates the form display to enable
   * the field so that it is not initially in the disabled fields section.
   */
  private static function createFieldActions(
    $field_storage_config,
    $field_config,
    $form_options
  ) {

    $field_config_created = self::createFieldActionsConfig(
      $field_storage_config, $field_config
    );

    if ($field_config_created) {

      $bundle = $field_config['bundle'];
      $field_name = $field_config['field_name'];
      $entity_type = $field_config['entity_type'];

      // Manage form display - enables the field.
      \Drupal::service('entity_display.repository')
        ->getFormDisplay($entity_type, $bundle)
        ->setComponent($field_name, $form_options)
        ->save();

      return TRUE;

    }

    return FALSE;

  }

  /**
   * Prepares the fields weights of a bundle by setting them to discrete
   * numbers. Previously the weight integers are 'all over the shop', with
   * any difference permitted between the field weights.
   * Rationalising and resetting the field weights is necessary so
   * that the Administrative Title field is the first field in a Paragraph,
   * and also so that the fields are correctly allocated to Field Groups by
   * ParagroupFieldGroupManager.
   */
  public static function prepareFieldWeights(
    $entity_type,
    $bundle,
    $start_weight = 1
  ) {

    $fields = \Drupal::service('entity_display.repository')
      ->getFormDisplay($entity_type, $bundle)
      ->getComponents();

    $fields_by_weight =
      self::orderFieldsByWeight($fields);

    $fields_by_rational_weight =
      self::orderFieldsByRationalWeight(
        $fields_by_weight, $start_weight
      );

    $reset_fields =
      self::resetFieldsWithNewWeights(
        $fields_by_rational_weight
      );

    $updated_weights =
      self::updateFieldWeights(
        $entity_type, $bundle, $reset_fields
      );

    return $updated_weights;

  }

  /**
   * Creates a field, by first updating a bundle's field weights, then
   * creating the field.
   */
  public static function createField(
    $field_storage_config,
    $field_config,
    $form_options
  ) {

    $bundle = $field_config['bundle'];
    $entity_type = $field_config['entity_type'];

    self::prepareFieldWeights($entity_type, $bundle);

    $field_created = self::createFieldActions(
      $field_storage_config, $field_config, $form_options
    );

    return $field_created;

  }

  /**
   * Deletes a field by deleting it's FieldConfig info, so that the field
   * is no longer set to the bundle. This deletes all the content stored
   * for that bundle. But the field's general FieldStorageConfig is still in
   * the database.
   */
  public static function deleteField($entity_type, $bundle, $field_name) {

    $load_field_config =
      FieldConfig::loadByName($entity_type, $bundle, $field_name);

    if ($load_field_config) {
      $load_field_config->delete();
      return TRUE;
    }

    return FALSE;

  }

  /**
   * Uses the general field creation system in this class to create an
   * Administrative Title field.
   */
  public static function createParagraphAdminTitleField($bundle) {

    $field_storage_config = [
      'field_name' => 'paragroup_admin_title',
      'entity_type' => 'paragraph',
      'type' => 'string',
      'cardinality' => 1,
    ];

    $field_config = [
      'field_name' => 'paragroup_admin_title',
      'entity_type' => 'paragraph',
      'bundle' => $bundle,
      'label' => 'Administrative Title',
      'field_type' => 'string',
    ];

    $form_options = [
      'type' => 'string_textfield',
      'weight' => 0,
    ];

    $field_created = self::createField(
      $field_storage_config, $field_config, $form_options
    );

    return $field_created;

  }

  /**
   * Uses the general field deletion system in this class to delete an
   * Administrative Title field.
   */
  public static function deleteParagraphAdminTitleField($bundle) {

    $field_deleted = self::deleteField(
      'paragraph', $bundle, 'paragroup_admin_title'
    );

    return $field_deleted;

  }

}
