<?php

namespace Drupal\paragraph_group\Paragroup;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Class ParagroupFormData.
 *
 * @package Drupal\paragraph_group\Paragroup
 *
 * Helper class for ParagroupConfigForm; contains functions used to generate
 * checkboxes for this Settings Form.
 */
class ParagroupFormData {

  use StringTranslationTrait;

  protected $stringTranslation;

  /**
   * Constructor.
   */
  public function __construct(TranslationInterface $string_translation) {

    $this->stringTranslation = $string_translation;

  }

  /**
   * Create function for constructor.
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('string_translation')
    );

  }

  /**
   * Gets complete list of Paragraph types.
   */
  private function getParagraphTypes() {

    $paragraph_types =
      \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo('paragraph');

    if (isset($paragraph_types['from_library'])) {
      unset($paragraph_types['from_library']);
    }

    return $paragraph_types;

  }

  /**
   * Determines whether a Paragraph bundle currently has an
   * Administrative Title field.
   */
  private function hasAdminTitleField($machine_name) {

    $field = FieldConfig::loadByName(
      'paragraph', $machine_name, 'paragroup_admin_title'
    );

    if ($field) {
      return 'Yes';
    }

    return 'No';

  }

  /**
   * Formats an option array for getAdminTitlesOptions.
   */
  private function getParagraphOption($machine_name, $info) {

    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    $path = "/admin/structure/paragraphs_type/$machine_name/fields";
    $url = $base_url . $path;
    $manage_fields = "<a href='$url' target='_blank'>Manage Fields</a>";
    $has_field = $this->hasAdminTitleField($machine_name);

    $option = [
      'paragraph_type' => ['data' => $info['label'], 'class' => 'name-field'],
      'machine_name' => ['data' => $machine_name],
      'has_field' => ['data' => $this->t($has_field)],
      'manage' => ['data' => $this->t($manage_fields)],
    ];

    return $option;

  }

  /**
   * Gets the Manage Form Display link for getFieldGroupsOptions.
   */
  private function getContentTypeOptionLabel($type) {

    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    $path = "/admin/structure/types/manage/$type/form-display";
    $url = $base_url . $path;

    $label = "<a href='$url' target='_blank'>Manage Form Display</a>";

    return $label;

  }

  /**
   * Returns list of content types.
   */
  private function getContentTypes() {

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    return $types;

  }

  /**
   * Gets form display url paths for each entity type.
   */
  private function getDetailsWidgetOptionUrl($field_id) {

    $base_url = \Drupal::request()->getSchemeAndHttpHost();

    [$entity_type, $bundle, $field_name] = explode('.', $field_id);

    $path = '';

    switch ($entity_type) {

      case 'node':
        $path =
          "admin/structure/types/manage/$bundle/form-display";
        break;

      case 'paragraph':
        $path =
          "admin/structure/paragraphs_type/$bundle/form-display";
        break;

      case 'taxonomy_term';
        $path =
          "admin/structure/taxonomy/manage/$bundle/overview/form-display";
        break;

      case 'block_content';
        $path =
          "admin/structure/block/block-content/manage/$bundle/form-display";
        break;

      case 'comment';
        $path =
          "admin/structure/comment/manage/$bundle/form-display";
        break;

      case 'contact_message';
        $path =
          "admin/structure/contact/manage/$bundle/form-display";
        break;

      default:
        return NULL;

    }

    return $base_url . '/' . $path;

  }

  /**
   * Gets the name of the current edit widget for a paragrpah field.
   */
  private function getCurrentWidget($field_id) {

    [$entity_type, $bundle, $field_name] = explode('.', $field_id);

    $field = \Drupal::service('entity_display.repository')
      ->getFormDisplay($entity_type, $bundle)
      ->getComponent($field_name);

    $defs = \Drupal::service('plugin.manager.field.widget')
      ->getDefinitions();

    $current_widget = $field['type'];
    $current_widget_label = $defs[$current_widget]['label']->render();

    return $current_widget_label;

  }

  /**
   * Formats an option array for getDetailsWidgetOptions.
   */
  private function getDetailsWidgetOption($field_id, $field_config) {

    $label = $field_config->get('label');
    $url = $this->getDetailsWidgetOptionUrl($field_id);
    $manage = '';

    if ($url) {
      $manage = "<a href='$url' target='_blank'>Manage Form Display</a>";
    }
    else {
      $manage = "N / A";
    }

    $current_widget = $this->getCurrentWidget($field_id);

    $option = [
      'paragraph_field' => ['data' => $label, 'class' => 'name-field'],
      'machine_name' => ['data' => $field_id],
      'current_widget' => ['data' => $current_widget],
      'manage' => ['data' => $this->t($manage)],
    ];

    return $option;

  }

  /**
   * Adds metadata to list of Paragraph fields generated by getFieldConfig.
   */
  private function getFieldConfigEntities() {

    $field_config_ids = $this->getFieldConfig();

    $field_config_entities = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->loadMultiple($field_config_ids);

    return $field_config_entities;

  }

  /**
   * Determines whether or not a bundle currently has any field groups.
   */
  private function hasFieldGroups($bundle) {

    $groups = field_group_info_groups('node', $bundle, 'form', 'default');

    if (!empty($groups)) {
      return 'Yes';
    }

    return 'No';

  }

  /**
   * Formats an option array for getFieldGroupsOptions.
   */
  private function getContentTypeOption($node_type, $name, $label) {

    $has_field_groups = $this->hasFieldGroups($node_type);

    $option = [
      'content_type' => ['data' => $name, 'class' => 'name-field'],
      'machine_name' => ['data' => $node_type],
      'has_field_groups' => ['data' => $has_field_groups],
      'manage' => ['data' => $this->t($label)],
    ];

    return $option;

  }

  /**
   * Array data function returning info about Settings form structure and
   * form container elements.
   */
  public function getFormStructure() {

    $form_structure = [
      'admin_titles_section' => 'admin_titles_boxes',
      'details_widget_section' => 'details_widget_boxes',
      'field_groups_section' => 'field_groups_boxes',
      'theme_mods_section' => 'theme_mods_boxes',
    ];

    return $form_structure;

  }

  /**
   * Returns a list of all Paragraph fields in website which can have a
   * Paragraph Details widget added to them.
   */
  public function getFieldConfig() {

    $field_config_ids = \Drupal::entityQuery('field_config')
      ->accessCheck(FALSE)
      ->condition('field_type', 'entity_reference_revisions')
      ->condition('settings.handler', 'default:paragraph')
      ->execute();

    return $field_config_ids;

  }

  /**
   * Returns a list of Paragraph Details widget options.
   */
  public function getDetailsWidgetOptions() {

    $field_config_entities = $this->getFieldConfigEntities();

    $options = [];

    foreach ($field_config_entities as $field_id => $field_config) {

      $option = $this->getDetailsWidgetOption($field_id, $field_config);
      $option_id = str_replace('.', '', $field_id);
      $options[$option_id] = $option;

    }

    return $options;

  }

  /**
   * Returns a list of Paragraph types used for Administrative Titles options.
   */
  public function getAdminTitlesOptions() {

    $paragraph_types = $this->getParagraphTypes();
    $options = [];

    foreach ($paragraph_types as $machine_name => $info) {
      $option = $this->getParagraphOption($machine_name, $info);
      $options[$machine_name] = $option;
    }

    return $options;

  }

  /**
   * Returns a list of disabled Field Group checkboxes.
   */
  public function getDisabledFieldGroupBoxes() {

    $types = array_keys($this->getContentTypes());
    $boxes = [];

    foreach ($types as $type) {

      $proceed = ParagroupFieldGroupManager::proceedWithCreate($type);

      if (!$proceed) {
        $boxes[$type] = ['#disabled' => TRUE];
      }

    }

    return $boxes;

  }

  /**
   * Returns a list of content types used for Field Groups options.
   */
  public function getFieldGroupsOptions() {

    $types = $this->getContentTypes();
    $options = [];

    foreach ($types as $type => $node_type) {

      $name = $node_type->get('name');
      $label = $this->getContentTypeOptionLabel($type);
      $options[$type] = $this->getContentTypeOption($type, $name, $label);

    }

    return $options;

  }

}
