<?php

namespace Drupal\paragraph_group\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraph_group\Paragroup\ParagroupBatch;
use Drupal\paragraph_group\Paragroup\ParagroupFormData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ParagroupConfigForm.
 *
 * @package Drupal\paragraph_group\Paragroup
 *
 * Generates module settings form for paragraph_group.form route.
 */
class ParagroupConfigForm extends ConfigFormBase {

  protected $batch;
  protected $formData;

  /**
   * Constructor.
   */
  public function __construct(
    ParagroupBatch $batch_obj,
    ParagroupFormData $form_data
  ) {

    $this->batch = $batch_obj;
    $this->formData = $form_data;

  }

  /**
   * Create function for constructor.
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('paragraph_group.batch'),
      $container->get('paragraph_group.form_data')
    );

  }

  /**
   * Returns whether or not the section Details element should be open
   * on page load.
   */
  private function getSectionDetailsOpen($vals) {

    $open = FALSE;

    if(is_array($vals) && !empty($vals)) {

      foreach ($vals as $key => $val) {

        if ($val) {
          $open = TRUE;
          break;
        }

      }

    }

    return $open;

  }

  /**
   * Description text for each section, including Usage Guidelines.
   */
  private function getDescription($section) {

    $descriptions = [

      'theme_mods_section' =>
      '<span class="paragroup intro">Paragraph Group module is now ' .
      'compatible with 3 Administration Themes: the ' .
      '<a href="https://www.drupal.org/project/claro">Claro</a>, ' .
      '<a href="https://www.drupal.org/project/gin">Gin</a> and ' .
      '<a href="https://www.drupal.org/project/seven">Seven</a> themes.</span>' .
      '<span class="paragroup more-info">To ensure optimal compatibility of ' .
      'these supported themes with the Field Groups and Paragraphs modules, ' .
      'as well as with the Paragraph Group module generally, please review ' .
      'our Usage Guidelines and choose from the options below.</span>' .
      '<details class="paragroup guidelines">' .
      '<summary>Usage Guidelines</summary>' .
      '<ul class="paragroup explan">' .
      '<li>Although Seven theme works well with Drupal versions 7-8, it is ' .
      'now deprecated and currently buggy out of the box with Drupal 9-10, ' .
      'independently of Paragraph Group module.</li>' .
      '<li>The issues with Seven theme can potentially be fixed by tweaking ' .
      ' / customizing placement of blocks; but for the moment, while this ' .
      'module supports the Seven theme in Drupal 9, we recommmend using the ' .
      'Claro or Gin themes instead.</li>' .
      '<li>The more modern Claro (Drupal\'s new default administration ' .
      'theme) and Gin themes are supported and recommended for Drupal ' .
      'versions 9 and 10.</li>' .
      '<li>Overall, without the override options below, recent changes to ' .
      'Drupal\'s default administration themes (Seven and Claro) potentially ' .
      'make the Paragraphs and Field Groups modules more challenging to use. ' .
      'Nevertheless, with Paragraph Group and the options below, these ' .
      'functionalities become stronger and better integrated than ever for ' .
      'Drupal 9 and 10, and together continue to bring structure and order ' .
      'to your unruly content authoring and editing forms!</li>' .
      '</ul></details>',

      'details_widget_section' =>
      '<span class="paragroup intro">The Paragraph Details widget uses the ' .
      'expandable HTML Details element, making nested paragraph groups ' .
      'easier to navigate and edit. </span>' .
      '<span class="paragroup more-info">The HTML5 Details element is the ' .
      'native HTML successor to the jQueryUI Accordion widget. It is used ' .
      'throughout this page to contain its 4 main sections and their ' .
      'usage guidelines - try clicking them for yourself!</span>' .
      '<details class="paragroup guidelines">' .
      '<summary>Usage Guidelines</summary>' .
      '<ul class="paragroup explan">' .
      '<li>To ensure all your Paragraph content is easily editable when ' .
      'nested, we recommend checking the Select All box in the top left ' .
      'corner of the table below, to apply the Paragraph Details widget to ' .
      'all your website\'s Paragraph fields.</li>' .
      '<li>When unchecking a Paragraph Field, it\'s edit widget will be ' .
      'restored to the Paragraphs Legacy (classic) widget.</li>' .
      '<li>The Machine Name column below shows which Entity and Bundle a ' .
      'field belongs to, and is formatted [entity.bundle.field_name]. So ' .
      'for general Content Types, this will be ' .
      '[node.content_type_name.field_name].</li>' .
      '<li>Your website content itself is not affected by which widget ' .
      'is used on your edit pages, so the field widgets below can be ' .
      'updated without concern for modifying or deleting site content.</li>' .
      '</ul></details>',

      'admin_titles_section' =>
      '<span class="paragroup intro">You can enter a summary into this ' .
      'field that will appear in the Paragraph Details widget, making ' .
      'paragraphs easier to locate when editing.</span>' .
      '<details class="paragroup guidelines">' .
      '<summary>Usage Guidelines</summary>' .
      '<ul class="paragroup explan"><li>Without the Administrative Title ' .
      'Field, the Paragraph Details widget will (following the architectural ' .
      'conventions of the Paragraphs module) use the first non-empty text ' .
      'field appearing at the top of the Paragraph\'s edit widget for its ' .
      'summary text instead.</li>' .
      '<li><b>If a Paragraph Type currently has an Administrative Title ' .
      'Field as shown in the table below and is subsequently unchecked and ' .
      'saved, the field and all content saved in it for this Paragraph Type ' .
      'will be deleted.</b></li></ul></details>',

      'field_groups_section' =>
      '<span class="paragroup intro">Field Groups organise groups of your ' .
      'fields into vertical tabs, making Content Types with large field sets ' .
      'easier to manage.</span>' .
      '<span class="paragroup more-info">This functionality is simple to use ' .
      'and automatically adds a sensible set of tailored Field Groups to ' .
      'your selected Content Types. For more detailed information, please ' .
      'take some time to read the Usage Guidelines below.</span>' .
      '<details class="paragroup guidelines">' .
      '<summary>Usage Guidelines</summary>' .
      '<div class="paragroup container">' .
      '<div class="paragroup info-section">' .
      '<span class="info-section-title">General Information:</span>' .
      '<ul class="paragroup explan">' .
      '<li>When checked, a default set of Field Groups will be added to a ' .
      'Content Type, which can and should be further customized as ' .
      'convenient.</li>' .
      '<li>The intention of this module\'s Field Groups ' .
      'functionality is to use automation to introduce users to the Field ' .
      'Groups module and how it can tidy up your edit forms, rather than to ' .
      'recommend our own Field Group configuration.</li>' .
      '<li>As Field Groups do not store any content, website content itself ' .
      'is unaffected by creation, deletion or other usage of Field ' .
      'Groups.</li>' .
      '<li>The table below shows whether a Content Type currently has any ' .
      'Field Groups maintained by this module.</li></ul></div>' .

      '<div class="paragroup info-section">' .
      '<span class="info-section-title">Business Rules for Automatic ' .
      'Tailoring of Field Groups:</span>' .
      '<ul class="paragroup explan">' .
      '<li>If a Content Type does not have a checkbox in the table below, ' .
      'then the Content Type lacks the quantity or diversity of fields ' .
      'needed for it to have Field Groups maintained by this module.</li>' .
      '<li>To have Field Groups applied by this module, a Content Type must ' .
      'have at least 3 fields, which can include at minimum the ' .
      'Title and Body fields in addition to one or more others (not ' .
      'including layout section fields).</li>' .
      '<li>This module will try to create 4 Field Groups per Content Type, ' .
      'these being: <b>Main, Paragraphs, Media Uploads and ' .
      'Miscellaneous</b>. These will be in Vertical Tabs.</li>' .
      '<li>If the Content Type lacks Paragraph or File / Media Upload ' .
      'fields, it will not create Field Groups for these fields.</li>' .
      '<li>Title and Body fields will be put in the Main group.</li>' .
      '<li>Fields which are not Paragraphs or Media Upload fields will ' .
      'be put in the Miscellaneous group, unless the Content Type has no ' .
      'Body field, in which case the miscellaneous fields will be put in ' .
      'the Main group with the Title field. In that case, there will be ' .
      'no Miscellaneous group.</li>' .
      '<li>If this process only generates one group, the Content Type ' .
      'will not have a checkbox below, and Field Groups will not be applied ' .
      'to it by this module. A minimum of 2 Field Groups is required.</li>' .
      '</ul></div>' .

      '<div class="paragroup info-section">' .
      '<span class="info-section-title">Use Cases where a Content Type\'s ' .
      'Field Groups are Deleted:</span>' .
      '<ul><li>All a Content Type\'s current Field Groups are ' .
      'deleted by updates to that Content Type in the table below, ' .
      'either when unchecking to delete groups, or when checking to delete ' .
      'current groups and create the new groups.</li>' .
      '<li>When the field configuration of a Content Type currently ' .
      'ticked below is modified, by either adding or deleting a field from ' .
      'the Content Type; then Paragraph Group needs to recreate its ' .
      'default Field Groups for that Content Type in order to account for ' .
      'the new configuration, and will delete any Field Group ' .
      'customizations previously added to the Content Type.</li>' .
      '<li>If there are not enough fields left in the Content Type after ' .
      'your modifications as specified in the Business Rules above, then all ' .
      'Field Groups are deleted from the Content Type. They will be ' .
      'replaced if sufficient fields are added again later, and the Content ' .
      'Type remains ticked in the table below.</li></ul></div>' .

      '<div class="paragroup info-section">' .
      '<span class="info-section-title">Maintaining your Field Group ' .
      'Customizations:</span>' .
      '<ul><li>If you would like to preserve your own Field Group ' .
      'customizations when adding or removing fields from a Content Type, ' .
      'you can do so by uninstalling this module, then making your changes. ' .
      'When you subsequently reinstall this module, you can then mostly ' .
      'restore the same configuration on this page (though it will not be ' .
      'automatically remembered). As noted previously however, ticking and ' .
      'saving the Content Type you wish to preserve in the table below will ' .
      'restore Paragraph Group\'s default Field Group configuration for that ' .
      'Content Type and remove your Field Group customizations.</li>' .
      '<li>So maintaining your Field Group customizations means firstly ' .
      'uninstalling this module, and then not selecting the Content Types ' .
      'you have applied Field Group customizations to again in the table ' .
      'below when you reinstall this module. Reselecting these Content Types ' .
      'and having them managed by Paragraph Group entails losing ' .
      'your Field Group customizations.</li></ul></div>' .
      '</div></details>',

    ];

    return $descriptions[$section];

  }

  /**
   * Returns the tableselect element as the content for each section.
   */
  private function getContent($options, $header, $default_value, $empty) {

    $options = is_array($options) ? $options : [];
    $default_value = is_array($default_value) ? $default_value : [];

    $tableselect = [
      '#type' => 'tableselect',
      '#options' => $options,
      '#header' => $header,
      '#default_value' => $default_value,
      '#empty' => $empty,
    ];

    return $tableselect;

  }

  /**
   * Returns a list of Administration Theme styling options.
   */
  public function getThemeModsOptions() {

    $fwf_title =
      'Full Width Claro Forms (highly recommended for Claro theme)';

    $fwf_desc =
      'Claro currently shows edit forms within a central text section on all ' .
      'devices. To ensure optimal compatibility with Paragraphs and Field ' .
      'Groups, it is highly recommended to disable this in the Claro theme ' .
      'by checking this option in order to utilize the empty screen space.';

    $full_width_forms = [
      'option' => ['data' => $fwf_title, 'class' => 'name-field'],
      'desc' => ['data' => $fwf_desc],
    ];

    $sc_title =
      'Move page configuration sidebar to bottom of edit pages ' .
      '(optional for Seven and Claro themes; requires Full Width Claro Forms)';

    $sc_desc =
      'On edit pages, the Seven and Claro themes have a configuration ' .
      'sidebar on the right of screen. If your website utilizes ' .
      'large sets of highly nested paragraphs, then you might ' .
      'also like to move this sidebar to the bottom of the screen, ' .
      'in addition to the above option, by clicking this option ' .
      'in order to fully maximise the horizontal screen space available ' .
      'for your nested paragraphs.';

    $sidebar_config = [
      'option' => ['data' => $sc_title, 'class' => 'name-field'],
      'desc' => ['data' => $sc_desc],
    ];

    $options = [
      'full_width_forms' => $full_width_forms,
      'sidebar_config' => $sidebar_config,
    ];

    return $options;

  }

  /**
   * Gets the Paragraph Details widget section tableselect.
   */
  private function getThemeModsContent($header, $default_value) {

    $title = 'No options.';
    $options = $this->getThemeModsOptions();
    $content = $this->getContent($options, $header, $default_value, $title);

    return $content;

  }

  /**
   * Gets the Paragraph Details widget section tableselect.
   */
  private function getDetailsWidgetContent($header, $default_value) {

    $title = 'No Paragraph fields found.';
    $options = $this->formData->getDetailsWidgetOptions();
    $content = $this->getContent($options, $header, $default_value, $title);

    return $content;

  }

  /**
   * Gets the Administrative Titles section tableselect.
   */
  private function getAdminTitlesContent($header, $default_value) {

    $title = 'No Paragraph types found.';
    $options = $this->formData->getAdminTitlesOptions();
    $content = $this->getContent($options, $header, $default_value, $title);

    return $content;

  }

  /**
   * Gets the Field Groups section tableselect.
   */
  private function getFieldGroupsContent($header, $default_value) {

    $title = 'No content types found.';
    $options = $this->formData->getFieldGroupsOptions();
    $content = $this->getContent($options, $header, $default_value, $title);

    if (!empty($options)) {

      $disabled_boxes = $this->formData->getDisabledFieldGroupBoxes();
      $content = array_merge($content, $disabled_boxes);

    }

    return $content;

  }

  /**
   * Form data for the Paragraph Details widget section.
   */
  private function detailsWidget() {

    $form = [];
    $config = $this->config('paragraph_group.settings');
    $title = 'Use the <b>Paragraph Details</b> widget to edit your Paragraphs';
    $description = $this->getDescription('details_widget_section');
    $default_value = $config->get('paragraph_group.details_widget_boxes') ?? [];

    $form['details_widget_group'] = [
      '#type' => 'item',
    ];

    $header = [
      'paragraph_field' => ['data' => $this->t('Paragraph Field')],
      'machine_name' => ['data' => $this->t('Machine Name')],
      'current_widget' => ['data' => $this->t('Current Widget')],
      'manage' => ['data' => $this->t('Manage / customize')],
    ];

    $content = $this->getDetailsWidgetContent($header, $default_value);

    $form['details_widget_group']['details_widget_boxes'] = [
      '#type' => 'details',
      '#title' => $this->t($title),
      '#description' => $this->t($description),
      '#open' => $this->getSectionDetailsOpen($default_value),
      'details_widget_boxes' => $content,
    ];

    return $form;

  }

  /**
   * Form data for the Administrative Titles section.
   */
  private function adminTitles() {

    $form = [];
    $config = $this->config('paragraph_group.settings');
    $title = 'Add an <b>Administrative Title Field</b> to your Paragraphs';
    $description = $this->getDescription('admin_titles_section');
    $default_value = $config->get('paragraph_group.admin_titles_boxes');

    $form['admin_titles_group'] = [
      '#type' => 'item',
    ];

    $header = [
      'paragraph_type' => ['data' => $this->t('Paragraph Type')],
      'machine_name' => ['data' => $this->t('Machine Name')],
      'has_field' => ['data' => $this->t('Has Administrative Title Field')],
      'manage' => ['data' => $this->t('Manage / customize')],
    ];

    $content = $this->getAdminTitlesContent($header, $default_value);

    $form['admin_titles_group']['admin_titles_boxes'] = [
      '#type' => 'details',
      '#title' => $this->t($title),
      '#description' => $this->t($description),
      '#open' => $this->getSectionDetailsOpen($default_value),
      'admin_titles_boxes' => $content,
    ];

    return $form;

  }

  /**
   * Gets Field Groups checkboxes for the fieldGroups function.
   */
  private function getFieldGroupsBoxes($text) {

    $config = $this->config('paragraph_group.settings');
    $default_value = $config->get('paragraph_group.field_groups_boxes');

    $header = [
      'content_type' => ['data' => $this->t('Content Type')],
      'machine_name' => ['data' => $this->t('Machine Name')],
      'has_field_groups' => ['data' => $this->t('Has Field Groups')],
      'manage' => ['data' => $this->t('Manage / customize')],
    ];

    $content = $this->getFieldGroupsContent($header, $default_value);

    $boxes = [
      '#type' => 'details',
      '#title' => $this->t($text['title']),
      '#description' => $this->t($text['desc']),
      '#open' => $this->getSectionDetailsOpen($default_value),
      'field_groups_boxes' => $content,
    ];

    return $boxes;

  }

  /**
   * Form data for the Field Groups section.
   */
  private function fieldGroups() {

    $form = [];
    $description = $this->getDescription('field_groups_section');

    $title =
      'Use <b>Field Groups</b> to structure edit forms for your Content Types';

    $text = [
      'title' => $title,
      'desc' => $description,
    ];

    $form['field_groups_group'] = [
      '#type' => 'item',
    ];

    $form['field_groups_group']['field_groups_boxes'] =
      $this->getFieldGroupsBoxes($text);

    return $form;

  }

  /**
   * Form data for the Theme Compatibility Modifications section.
   */
  private function themeMods() {

    $form = [];
    $config = $this->config('paragraph_group.settings');
    $title = 'Improve <b>Administration Theme styling</b> for optimal compatibility';
    $description = $this->getDescription('theme_mods_section');
    $default_value = $config->get('paragraph_group.theme_mods_boxes');

    $form['theme_mods_group'] = [
      '#type' => 'item',
    ];

    $header = [
      'option' => ['data' => $this->t('Option')],
      'desc' => ['data' => $this->t('Description')],
    ];

    $content = $this->getThemeModsContent($header, $default_value);

    $form['theme_mods_group']['theme_mods_boxes'] = [
      '#type' => 'details',
      '#title' => $this->t($title),
      '#description' => $this->t($description),
      '#open' => $this->getSectionDetailsOpen($default_value),
      'theme_mods_boxes' => $content,
    ];

    return $form;

  }

  /**
   * Gets the form data for all 3 sections.
   */
  private function mainCheckboxElements(&$form) {

    $details_widget = $this->detailsWidget();
    $form['main_checkboxes']['details_widget_group'] =
      $details_widget['details_widget_group'];

    $admin_titles = $this->adminTitles();
    $form['main_checkboxes']['admin_titles_group'] =
      $admin_titles['admin_titles_group'];

    $field_groups = $this->fieldGroups();
    $form['main_checkboxes']['field_groups_group'] =
      $field_groups['field_groups_group'];

    $theme_mods = $this->themeMods();
    $form['main_checkboxes']['theme_mods_group'] =
      $theme_mods['theme_mods_group'];

    return $form;

  }

  /**
   * Gets the page form data, including introductory and suffix text, with
   * the form content from mainCheckboxElements in the middle.
   */
  private function mainCheckboxes($form, &$form_state) {

    $title =
    '<div class="elements-title">' .
    'The Paragraph Group module offers 4 integrated ways to significantly ' .
    'upgrade the content authoring experience of your website:' .
    '</div>';

    $form['main_checkboxes'] = [
      '#type' => 'item',
      '#markup' => $this->t($title),
      '#prefix' => '<div class="paragroup elements">',
      '#suffix' => '</div>',
    ];

    $form = $this->mainCheckboxElements($form);

    $backup_text =
    '<div class="elements-backup">' .
    '<b>Backup Your Website</b><br/>' .
    'If updating your production website, please backup your ' .
    'database and site configuration before saving a new configuration, ' .
    'particularly when updating the first 3 sections above.' .
    '<br/><br/>' .
    '<b>Recommended Configuration</b><br/>' .
    'You can implement our recommended configuration by ' .
    '<a id="pg-rec-config-link" href="#">clicking here</a> to select all the ' .
    'checkboxes in the first 3 sections above, as well as "Full Width Claro ' .
    'Forms" in the fourth section above, to see how the Paragraph Group ' .
    'module can improve all of your Paragraph and content forms!</div>';

    $form['main_checkboxes']['backup'] = [
      '#type' => 'item',
      '#markup' => $this->t($backup_text),
    ];

    return $form;

  }

  /**
   * Sets message when form submitted without changes to current configuration.
   */
  private function noChanges() {

    $msg = "No changes were made to the configuration.";
    \Drupal::messenger()->addStatus($msg);

  }

  /**
   * Gets the old and new form config / form state, returning
   * these in an array.
   */
  private function getOldAndNewConfig(FormStateInterface $form_state) {

    $old_config = [];
    $new_config = [];

    $form_structure = $this->formData->getFormStructure();
    $config = $this->config('paragraph_group.settings');

    foreach ($form_structure as $section => $boxes) {

      $boxes_name = 'paragraph_group.' . $boxes;
      $old_config[$boxes_name] = $config->get($boxes_name);
      $new_config[$boxes_name] = $form_state->getValue($boxes);

    }

    return [$old_config, $new_config];

  }

  /**
   * If the submission is to be completed, do so by running the Batch API.
   */
  private function submitActions($old_and_new_config) {

    $batch = $this->batch->getBatchData($old_and_new_config);

    if (!empty($batch['operations'])) {

      batch_set($batch);
      return TRUE;

    }

    return FALSE;

  }

  /**
   * When form submission is proceeding, saves checkbox states after Batch
   * API operations are complete.
   */
  private function submitProceed($old_and_new_config) {

    $this->submitActions($old_and_new_config);

    $new_config = $old_and_new_config[1];
    $config = $this->config('paragraph_group.settings');

    foreach ($new_config as $key => $val) {
      $config->set($key, $val);
    }

    $config->save();

    $msg = "Configuration changes saved successfully.";
    \Drupal::messenger()->addStatus(t($msg));

  }

  /**
   * Helper function returning values from a multidimensional
   * array in a one dimensional array.
   */
  private function getArrayValues($array) {

    $values = [];

    foreach ($array as $value) {

      if (is_array($value)) {
        $values = array_merge($values, $this->getArrayValues($value));
      }
      else {
        $values[] = $value;
      }

    }

    return $values;

  }

  /**
   * Determines whether there is a difference between the old and new form
   * state configuration and whether to proceed with the form submission on
   * that basis. Function was necessary as simply comparing
   * $old_config != $new_config was insufficient for edge cases on new
   * Drupal site installations.
   */
  private function proceedState($old_and_new_config) {

    $old_config = $old_and_new_config[0];
    $new_config = $old_and_new_config[1];

    $old_vals = $this->getArrayValues($old_config);
    $new_vals = $this->getArrayValues($new_config);

    $count_old_vals = count(array_filter($old_vals));
    $count_new_vals = count(array_filter($new_vals));

    $no_vals = ($count_old_vals == 0 && $count_new_vals == 0);
    $no_changes = ($old_config == $new_config);

    if ($no_vals || $no_changes) {
      return FALSE;
    }

    return TRUE;

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'paragroup_config_form';

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    $page = $this->mainCheckboxes($form, $form_state);
    $form = array_merge($form, $page);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    return parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $old_and_new_config = $this->getOldAndNewConfig($form_state);

    if ($this->proceedState($old_and_new_config)) {
      $this->submitProceed($old_and_new_config);
    }
    else {
      $this->noChanges();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return [
      'paragraph_group.settings',
    ];

  }

}
