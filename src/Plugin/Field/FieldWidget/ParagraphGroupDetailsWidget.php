<?php

namespace Drupal\paragraph_group\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\InlineParagraphsWidget;

/**
 * Paragraphs Details widget.
 *
 * @FieldWidget(
 *   id = "paragraph_group_details_widget",
 *   label = @Translation("Paragraph Details"),
 *   description = @Translation("Extends Paragraphs Legacy (classic) widget by wrapping it in a details / accordion element."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ParagraphGroupDetailsWidget extends InlineParagraphsWidget {

  /**
   * Gets the non machine-name label / name for a Paragraph.
   */
  private function getParagraphLabel($machine_name) {

    $paragraph_types =
      \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo('paragraph');

    if (isset($paragraph_types[$machine_name])) {
      return $paragraph_types[$machine_name]['label'];
    }

    return FALSE;

  }

  /**
   * Gets the Summary text for a Details element, ensuring that the summary
   * is fewer than 256 characters in length.
   */
  private function getSummaryText(
    $element,
    $delta,
    $form_state
  ) {

    $parents = $element['#field_parents'];
    $field_name = $this->fieldDefinition->getName();
    $widget_state = static::getWidgetState($parents, $field_name, $form_state);

    if (isset($widget_state['paragraphs'][$delta]['entity'])) {

      $paragraphs_entity = $widget_state['paragraphs'][$delta]['entity'];
      $summary_items = $paragraphs_entity->getSummaryItems();

      foreach ($summary_items['content'] as $item) {

        if (mb_strlen($item) < 256) {
          return $item;
        }

      }

    }

    return NULL;

  }

  /**
   * Returns the prefixed Summary text.
   */
  private function getDetailSummary(
    $element,
    $delta,
    $form_state
  ) {

    $summary = $this->getSummaryText($element, $delta, $form_state);

    if ($summary) {
      return "Summary: $summary";
    }

    return 'Paragraph';

  }

  /**
   * Overrides the Element data used by the formElement function.
   */
  private function getElementData(
    $element,
    $delta,
    $form_state
  ) {

    $paragraph_type = $element['#paragraph_type'];
    $paragraph_label = $this->getParagraphLabel($paragraph_type);

    $type_text = "Paragraph Type: " . $paragraph_label;

    $detail_summary =
      $this->getDetailSummary($element, $delta, $form_state);

    $element['#type'] = 'details';
    $element['#title'] = $this->t($detail_summary);
    $element['#prefix'] .= "<span>$type_text</span>";

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    // Remove edit mode from summary.
    $old_summary = parent::settingsSummary();
    $new_summary = [];

    foreach ($old_summary as $key => $val) {

      if ($key != 2) {
        $new_summary[] = $val;
      }

    }

    return $new_summary;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = parent::settingsForm($form, $form_state);

    // Remove edit mode from settings form.
    $elements['edit_mode']['#type'] = 'hidden';

    return $elements;

  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {

    $element =
      parent::formElement($items, $delta, $element, $form, $form_state);

    // Setup form elements as details html elements:
    if (is_numeric($delta)) {

      $element =
        $this->getElementData($element, $delta, $form_state);

    }

    // Remove paragraph type info from within paragraph form element,
    // as just above we've added it to the prefix outside the details widget.
    unset($element['top']['paragraph_type_title']['info']);

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function formMultipleElements(
    FieldItemListInterface $items,
    array &$form,
    FormStateInterface $form_state
  ) {

    // The Paragraph Details widget only supports the default 'open' edit mode,
    // and improves it so that the other edit modes shouldn't be necessary.
    // So setting all form elements edit_mode to 'open'.
    $this->setSetting('edit_mode', 'open');

    $widget = parent::formMultipleElements($items, $form, $form_state);
    $attached = &$widget['#attached'];

    _paragraph_group_attach_details_widget($attached);

    return $widget;

  }

}
