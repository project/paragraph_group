<?php

/**
 * @file
 */

use Drupal\Core\Link;

/**
 * Helper function returning the current Drupal installation's main version number.
 */
function _paragraph_group_get_drupal_version() {

  $version = \Drupal::VERSION;
  $version = explode('.', $version);
  $version = $version[0];

  return $version;

}

/**
 * Helper function notifying whether the chosen administration
 * theme is permissable.
 */
function _paragraph_group_validate_admin_theme($admin_theme) {

  $compatible_themes = NULL;
  $version = _paragraph_group_get_drupal_version();

  if ($version == 9) {
    $compatible_themes = ['seven', 'claro', 'gin'];
  }
  elseif ($version == 10) {
    $compatible_themes = ['claro', 'gin'];
  }

  if (!in_array($admin_theme, $compatible_themes)) {
    return FALSE;
  }

  return TRUE;

}

/**
 * Adds the theme modification config, from the module config page,
 * to the attached drupalSettings variable for the main.js file to add
 * to the HTML element as css classes for theme styling modifications.
 */
function _paragraph_group_add_theme_mod_config(&$attached) {

  $config = \Drupal::config('paragraph_group.settings');
  $theme_mods_config = $config->get('paragraph_group.theme_mods_boxes');
  $version = _paragraph_group_get_drupal_version();

  $theme_mods_bool = [
    'sc' => boolval($theme_mods_config['sidebar_config']),
    'fwf' => boolval($theme_mods_config['full_width_forms']),
    'ver' => $version,
  ];

  foreach ($theme_mods_bool as $key => $val) {

    if ($val) {
      $attached['drupalSettings']['paragraph_group_' . $key] = $val;
    }

  }

  return TRUE;

}

/**
 * Attaches the details widget library (js and css files) to the
 * form attached array passed via the $attached parameter.
 */
function _paragraph_group_attach_details_widget(&$attached) {

  // Attach paragraph_group js and css library, and js settings:
  $attached['library'][] = 'paragraph_group/main';
  $attached['drupalSettings']['paragraph_group_details_widget'] = TRUE;

  // Attach theme modfication config info as additional js settings:
  _paragraph_group_add_theme_mod_config($attached);

}

/**
 * Gets the list of bundles / content types whose edit
 * forms this module is currently formatting with field groups,
 * as set in the module's configuration page.
 */
function _paragraph_group_get_field_group_bundles() {

  $config = \Drupal::config('paragraph_group.settings');
  $boxes_name = 'paragraph_group.field_groups_boxes';
  $bundles = $config->get($boxes_name);

  if (is_array($bundles) && !empty($bundles)) {

    foreach ($bundles as $key => $val) {

      if (!$val) {
        unset($bundles[$key]);
      }

    }

  }

  return $bundles;

}

/**
 * Gets the human readable name of a Content Type
 * from its machine name.
 */
function _paragraph_group_get_content_type_name($bundle) {

  $type = \Drupal::entityTypeManager()
    ->getStorage('node_type')
    ->load($bundle);

  $name = $type->get('name');

  return $name;

}

/**
 * Shortcut to get the text for a link.
 */
function _paragraph_group_get_link($text, $route) {

  $link = Link::createFromRoute($text, $route);
  $link_render_array = $link->toRenderable();
  $link_render_array['#attributes']['target'] = '_blank';
  $link_markup = \Drupal::service('renderer')->render($link_render_array);
  $link_text = $link_markup->__toString();

  return $link_text;

}
